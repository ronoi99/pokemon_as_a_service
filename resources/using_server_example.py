# Step 1 run run_server.bat

POKEMON = {
    "pokadex_id": 1,
    "name": "Bulbasaur",
    "nickname": "Gavrial",
    "level": 20,
    "type": "GRASS",
    "skills": [
        "Tackle",
        "Growl",
        "Vine Whip",
        "Poison Powder",
        "Sleep Powder",
        "Take Down",
        "Razor Leaf",
        "Growth"
    ]
}


def add_pokemon(pokemon=None):
    import requests, json

    if not pokemon:
        pokemon = POKEMON

    result = requests.post("http://localhost/api/insert/", headers={'Content-Type': 'application/json'},
                           data=json.dumps(pokemon))
    print(result.content)


def search_pokemon(search_string="Bulba"):
    import requests

    result = requests.get("http://localhost/api/autocomplete/" + search_string)
    print(result.content)


if __name__ == "__main__":
    add_pokemon()
    search_pokemon()

