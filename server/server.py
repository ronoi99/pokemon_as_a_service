from flask import request, Flask
from server import backend


### Init code ###
app = Flask(__name__, static_url_path="")
ADD_REQUESTS = 0


@app.route("/api/autocomplete/<search_string>", methods=['GET'])
def get_pokemon(search_string):
    """
    Search for a pokemon with any field that contains the given search_string
    :param search_string: The search string
    :return: A string representation of an array with all the pokemons the query found
    """
    return str(backend.get_pokemon_from_elastic_search(search_string))


@app.route("/api/insert/", methods=['POST'])
def add_pokemon():
    """
    Add the pokemon to elastic search (if it is valid)
    If a pokemon with the same pokadex_id exists update its fields
    Gets the pokemon from the post data.

    Once in 1000 adds clear the cache
    :return: A string explaining the action result
    """
    global ADD_REQUESTS

    if not request.is_json():
        return "please provide a json"
    pokemon_json = request.get_json()

    ADD_REQUESTS += 1
    if ADD_REQUESTS % 1000 == 0:
        backend.clear_int_fields_cache()

    return backend.add_pokemon_to_elastic_search(pokemon_json)








