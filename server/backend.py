from elasticsearch import Elasticsearch
import requests

INDEX_NAME = "pokemon"

POKEMON_TYPES = ["ELECTRIC", "GROUND", "FIRE", "WATER", "WIND", "PSYCHIC", "GRASS"]
POKEMON_FIELDS = [("pokadex_id", int), ("name", str), ("nickname", str), ("level", int), ("type", str), ("skills", list)]
POKEMON_INT_FIELDS = [field_name for field_name, field_type in POKEMON_FIELDS if field_type == int]


def get_pokemon_from_elastic_search(search_string):
    """
    Search for a pokemon with any field that contains the given search_string
    :param search_string: The search string
    :return: An array with all the pokemons the query found
    """
    es = Elasticsearch()

    # better here and not as a constant for thread safety
    search_json = {
        "query": {
            "query_string": {
                "default_field": "*",
                "query": search_string + "*",
                "default_operator": "AND"
            }
        }
    }

    r = es.search(index=INDEX_NAME, body=search_json)
    hits = r['hits']['hits']
    if hits:
        return [hit['_source'] for hit in hits]
    return []


def add_pokemon_to_elastic_search(pokemon):
    """
    Add the pokemon to elastic search (if it is valid)
    If a pokemon with the same pokadex_id exists update its fields
    :param pokemon: The pokemon to add
    :return: A string explaining the action result
    """
    validation_error = pokemon_validator(pokemon)
    if validation_error:
        return validation_error

    es = Elasticsearch()
    result = es.index(index=INDEX_NAME, doc_type="_doc", body=pokemon, id=pokemon["pokadex_id"])
    if result:
        if result['result'] == "created":
            return "the pokemon was created successfully"
        if result['result'] == "updated":
            return "the pokemon was updated successfully"
    else:
        return "could not created pokemon"


def pokemon_validator(pokemon):
    """
    Validate that the given pokemon is in the right format
    :param pokemon: The pokemon to validate (a json)
    :return: A string with the validation error or an empty string if the pokemon is valid
    """
    for field_name, field_type in POKEMON_FIELDS:
        if field_name not in pokemon:
            return "pokemon must have the field " + field_name
        elif not isinstance(pokemon[field_name], field_type):
            return "The field {field} must be from type {t}".format(field=field_name, t=field_type.__name__)

    if pokemon["type"] not in POKEMON_TYPES:
        return pokemon["type"] + " is not a valid pokemon type"
    if any(map(lambda skill: not isinstance(skill, str), pokemon["skills"])):
        return "all pokemon skills must be from type str"

    return ""


def clear_int_fields_cache():
    """
    Elastic search cache automatically all field.
    We don't search on int field so the cache is not required
    """
    int_fields = ",".join(POKEMON_INT_FIELDS)
    requests.post("http://localhost:9200/{index}/_cache/clear?fields={int_fields}".format(index=INDEX_NAME,
                                                                                          int_fields=int_fields))
