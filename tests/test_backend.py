from server import backend
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError


POKEMON_1 = {
   "pokadex_id": 25,
   "name": "Pikachu",
   "nickname": "Baruh Ha Gever",
   "level": 60,
   "type": "ELECTRIC",
   "skills": [
       "Tail Whip",
       "Thunder Shock",
       "Growl",
       "Play Nice",
       "Quick Attack",
       "Electro Ball",
       "Thunder Wave"
   ]
}


def test_pokemon_validator():
    assert not backend.pokemon_validator(POKEMON_1)

    # missing field
    pokemon = POKEMON_1
    name = pokemon.pop("name")
    assert backend.pokemon_validator(pokemon)

    # field with wrong type
    pokemon["name"] = 123
    assert backend.pokemon_validator(pokemon)

    # illegal pokemon type
    pokemon["name"] = name
    pokemon["type"] = "Other"
    assert backend.pokemon_validator(pokemon)

    # skill with is not a string
    pokemon["type"] = "ELECTRIC"
    pokemon["skills"].append(1)
    assert backend.pokemon_validator(pokemon)
    pokemon["skills"].remove(1)


def test_add_pokemon_to_elastic_search():

    # adding
    es = Elasticsearch()
    try:
        es.delete(index=backend.INDEX_NAME, doc_type="_doc", id=POKEMON_1["pokadex_id"])
    except NotFoundError:
        pass

    result = backend.add_pokemon_to_elastic_search(POKEMON_1)
    assert result == "the pokemon was created successfully"

    # updating
    result = backend.add_pokemon_to_elastic_search(POKEMON_1)
    assert result == "the pokemon was updated successfully"


def test_get_pokemon_from_elastic_search():
    # found whole word
    result = backend.get_pokemon_from_elastic_search("Pikachu")
    assert len(result) > 0

    # found word prefix
    result = backend.get_pokemon_from_elastic_search("Pika")
    assert len(result) > 0

    # not found word suffix
    result = backend.get_pokemon_from_elastic_search("achu")
    assert len(result) == 0

    # not found
    result = backend.get_pokemon_from_elastic_search("asdfasdgfasdgsdgsdfgasfg")
    assert len(result) == 0
